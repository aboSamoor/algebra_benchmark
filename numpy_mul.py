#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""template.py: Description of what the module does."""

from argparse import ArgumentParser
import logging
import sys
from io import open
from os import path
from time import time
from glob import glob
from random import random

import numpy

__author__ = "Rami Al-Rfou"
__email__ = "rmyeid@gmail.com"

LOGFORMAT = "%(asctime).19s %(levelname)s %(filename)s: %(lineno)s %(message)s"


def matrix_mul(A, B):
  num_rows = len(A)
  tmp = len(A[0])
  assert tmp == len(B)
  num_cols = len(B[0])
  c = []
  for i in xrange(num_rows):
    c.append([])
    for j in xrange(num_cols):
      value = 0.0
      for k in xrange(tmp):
        value += A[i][k] * B[k][j]
      c[-1].append(value)
  return c


def random_matrix(rows, cols):
  c = []
  for i in xrange(rows):
    c.append([])
    for k in xrange(cols):
      c[-1].append(random())
  return c

def benchmark_1():
  iters = 100 
  num_examples = 16
  As = [numpy.array(random_matrix(num_examples, 320)) for i in range(iters)]
  Bs = [numpy.array(random_matrix(320, 32)) for i in range(iters)]
  Cs = []
  t_0 = time()
  for x in xrange(iters):
    Cs.append(numpy.dot(As[x], Bs[x]))
  print 'Benchmark 1 Example/sec: ', (iters * num_examples) / (time() - t_0)

def main(args):
  benchmark_1()


def debug(type_, value, tb):
  if hasattr(sys, 'ps1') or not sys.stderr.isatty():
    # we are in interactive mode or we don't have a tty-like
    # device, so we call the default hook
    sys.__excepthook__(type_, value, tb)
  else:
    import traceback
    import pdb
    # we are NOT in interactive mode, print the exception...
    traceback.print_exception(type_, value, tb)
    print("\n")
    # ...then start the debugger in post-mortem mode.
    pdb.pm()

if __name__ == "__main__":
  parser = ArgumentParser()
  parser.add_argument("-f", "--file", dest="filename", help="Input file")
  parser.add_argument("-l", "--log", dest="log", help="log verbosity level",
                      default="INFO")
  args = parser.parse_args()
  if args.log == 'DEBUG':
    sys.excepthook = debug
  numeric_level = getattr(logging, args.log.upper(), None)
  logging.basicConfig(level=numeric_level, format=LOGFORMAT)
  main(args)


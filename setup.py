#!/usr/bin/env python
# -*- coding: utf-8 -*-


from distutils.extension import Extension
from distutils.core import setup
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import numpy as np 


ext_modules = [
    Extension("cython_mul", ["cython_mul.pyx"], libraries=['blas'],
              include_dirs = [np.get_include()]),
    Extension("nn", ["nn.pyx"], libraries=['blas'],
              include_dirs = [np.get_include()]),
    Extension("polyglot", ["polyglot.pyx"], libraries=['blas'],
              include_dirs = [np.get_include()])
]

setup(
  name = 'Algebra',
  cmdclass={'build_ext': build_ext},
  ext_modules = ext_modules,
)

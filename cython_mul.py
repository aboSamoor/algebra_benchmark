#!/usr/bin/env pypy
# -*- coding: utf-8 -*-

"""template.py: Description of what the module does."""

from argparse import ArgumentParser
import logging
import sys
from io import open
from os import path
from time import time
from glob import glob
from random import random
from numpy import get_include, array, zeros, empty, ones
import numpy

import pyximport
#pyximport.install(setup_args={"include_dirs": get_include(),
#                              "libraries": [("blas", {})]},
#                  inplace=False)
from cython_mul import matrix_mul1, matrix_mul2
from cython_mul import matrix_mul3, blas2

__author__ = "Rami Al-Rfou"
__email__ = "rmyeid@gmail.com"

LOGFORMAT = "%(asctime).19s %(levelname)s %(filename)s: %(lineno)s %(message)s"


def random_matrix(rows, cols):
  c = []
  for i in xrange(rows):
    c.append([])
    for k in xrange(cols):
      c[-1].append(random())
  return c


num_examples = 16
iters = 1000 
As = [array(random_matrix(num_examples, 320)) for i in range(iters)]
Bs = [array(random_matrix(320, 32)) for i in range(iters)]

def benchmark_1():
  Cs = [empty((16, 32)) for i in range(iters)]
  t_0 = time()
  for x in xrange(iters):
    matrix_mul1(As[x], Bs[x], Cs[x])
  print 'Benchmark 1 Example/sec: ', (iters * num_examples) / (time() - t_0)
  return Cs

def benchmark_2():
  Cs = [empty((16, 32)) for i in range(iters)]
  t_0 = time()
  for x in xrange(iters):
    matrix_mul2(As[x], Bs[x], Cs[x])
  print 'Benchmark 2 Example/sec: ', (iters * num_examples) / (time() - t_0)
  return Cs

def benchmark_3():
  Cs = [empty((16, 32)) for i in range(iters)]
  Bs_F = [B.copy('F') for B in Bs]
  t_0 = time()
  for x in xrange(iters):
    matrix_mul3(As[x], Bs_F[x], Cs[x])
  print 'Benchmark 3 Example/sec: ', (iters * num_examples) / (time() - t_0)
  return Cs


def benchmark_blas2():
  Cs = [ones((16, 32)) for i in range(iters)]
  t_0 = time()
  for x in xrange(iters):
    blas2(As[x], Bs[x], Cs[x])
  print 'Blas 2 Example/sec: ', (iters * num_examples) / (time() - t_0)
  return Cs

def main(args):
  cs1 = benchmark_1()
  cs2 = benchmark_2()
  cs3 = benchmark_3()
  cs4 = benchmark_blas2()
  answer = numpy.dot(As[0], Bs[0])
  print 'Benchmark1', (cs1[0] == answer).all()
  print 'Benchmark2', (cs2[0] == answer).all()
  print 'Benchmark3', (cs3[0] == answer).all()
  diff = (cs4[0] - answer)
  print 'Blas2', (-0.00001 < diff).all() and (diff < 0.00001).all()


def debug(type_, value, tb):
  if hasattr(sys, 'ps1') or not sys.stderr.isatty():
    # we are in interactive mode or we don't have a tty-like
    # device, so we call the default hook
    sys.__excepthook__(type_, value, tb)
  else:
    import traceback
    import pdb
    # we are NOT in interactive mode, print the exception...
    traceback.print_exception(type_, value, tb)
    print("\n")
    # ...then start the debugger in post-mortem mode.
    pdb.pm()

if __name__ == "__main__":
  parser = ArgumentParser()
  parser.add_argument("-f", "--file", dest="filename", help="Input file")
  parser.add_argument("-l", "--log", dest="log", help="log verbosity level",
                      default="INFO")
  args = parser.parse_args()
  if args.log == 'DEBUG':
    sys.excepthook = debug
  numeric_level = getattr(logging, args.log.upper(), None)
  logging.basicConfig(level=numeric_level, format=LOGFORMAT)
  main(args)


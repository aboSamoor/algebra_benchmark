import numpy as np
cimport numpy as np
cimport cython
from cython.view cimport array as cvarray


DTYPE = np.double
ctypedef np.double_t DTYPE_t
ctypedef np.int_t ITYPE_t

#from scipy.linalg.blas cimport dgemm as cdgemm

cdef extern from "cblas.h" nogil:
    double ddot "cblas_ddot"(int N, double *X, int incX,double *Y, int incY)
    
    double cdgemm "cblas_dgemm"(char Order, 
                                char TransA,
                                char TransB,
                                int M, int N,
                                int K, double alpha, double *A, 
                                int lda, double *B, int ldb,
                                double beta, double *C, int ldc)

    void cdaxpy "cblas_daxpy"(const int N,
                              const double alpha,
                              const double *X,
                              const int incX,
                              double *Y,
                              const int incY)

cdef extern from "math.h" nogil:
      double ctanh "tanh"(double x)

cdef extern from "stdlib.h" nogil:
      int crandom "random"()

cdef float fasttanh(float x) nogil:
  if x > 4.97: return 1.0
  if x < -4.97: return -1.0
  cdef float x2 = x * x
  cdef float a = x * (135135.0 + x2 * (17325.0 + x2 * (378.0 + x2)))
  cdef float b = 135135.0 + x2 * (62370.0 + x2 * (3150.0 + x2 * 28.0))
  return a / b


@cython.boundscheck(False)
@cython.wraparound(False)
def train(model, ngrams_, alpha_):
  cdef DTYPE_t [:, ::1] W1 = model.W1
  cdef DTYPE_t [:, ::1] W2 = model.W2
  cdef DTYPE_t [:, ::1] vectors = model.vectors
  cdef int [:, ::1] ngrams = ngrams_

  cdef int context = model.context
  cdef int batch = model.batch
  cdef int size = model.vectors.shape[1]
  cdef int voc_size = model.vectors.shape[0]
  cdef int index_width = (context * 2 + 1)
  cdef int width = index_width * size
  cdef int hidden = model.W1.shape[1]

  cdef DTYPE_t alpha = alpha_

  cdef DTYPE_t[:, ::1] dW1 = cvarray(shape=model.W1.shape, itemsize=sizeof(DTYPE_t), format="d")
  cdef DTYPE_t[:, ::1] dW2 = cvarray(shape=model.W2.shape, itemsize=sizeof(DTYPE_t), format="d")

  cdef DTYPE_t[:, ::1] x1 = cvarray(shape=(batch, width), itemsize=sizeof(DTYPE_t), format="d")
  cdef DTYPE_t[:, ::1] x2 = cvarray(shape=(batch, width), itemsize=sizeof(DTYPE_t), format="d")
  cdef DTYPE_t[:, ::1] dx1 = cvarray(shape=(batch, width), itemsize=sizeof(DTYPE_t), format="d")
  cdef DTYPE_t[:, ::1] dx2 = cvarray(shape=(batch, width), itemsize=sizeof(DTYPE_t), format="d")

  cdef DTYPE_t[:, ::1] v1 = cvarray(shape=(batch, hidden), itemsize=sizeof(DTYPE_t), format="d")
  cdef DTYPE_t[:, ::1] v2 = cvarray(shape=(batch, hidden), itemsize=sizeof(DTYPE_t), format="d")
  cdef DTYPE_t[:, ::1] diff = cvarray(shape=(batch, hidden), itemsize=sizeof(DTYPE_t), format="d")

  cdef DTYPE_t[:, ::1] tmp2 = cvarray(shape=(batch, 1), itemsize=sizeof(DTYPE_t), format="d")

  cdef DTYPE_t[:, ::1] tanh_err1 = cvarray(shape=(batch, hidden), itemsize=sizeof(DTYPE_t), format="d")
 
  cdef DTYPE_t[:, ::1] tanh_err2 = cvarray(shape=(batch, hidden), itemsize=sizeof(DTYPE_t), format="d")

  cdef int rows_no = ngrams.shape[0]
  cdef int current_row = 0

  cdef int cur_col = 0
  cdef int cur_row = 0
  cdef int index1 = 0
  cdef int index2 = 0
  cdef int[:]  rand_index = cvarray(shape=(batch, ), itemsize=sizeof(int), format="i")

  cdef int i = 0
  cdef int j = 0
  cdef int k = 0


  with nogil:
    while (current_row + batch) < rows_no:

      # prepare x1, x2
      for i in xrange(batch):
        cur_row = current_row + i
        for j in xrange(index_width):
          cur_col = j * size
          index1 = ngrams[cur_row, j]
          index2 = index1
          if j == context:
            index2 = crandom() % voc_size
            rand_index[i] = index2
          for k in xrange(size):
            x1[i, cur_col + k] = vectors[index1, k]
            x2[i, cur_col + k] = vectors[index2, k]

      # learn the derivatives
      batch_train(x1, x2, W1, W2, batch, width, hidden,
                  dW1, dW2, dx1, dx2,
                  v1, v2, diff, tmp2, tanh_err1, tanh_err2)

      # ---------------
      # Update Params -
      # ---------------

      # update W1
      for i in xrange(width):
        for j in xrange(hidden):
          W1[i, j] -= alpha * dW1[i,j]
      
      # update W2
      for j in xrange(hidden):
        W2[j, 0] -= alpha * dW2[j, 0]

      # update vectors
      for i in xrange(batch):
        cur_row = current_row + i
        for j in xrange(index_width):
          cur_col = j * size
          index1 = ngrams[cur_row, j]
          index2 = index1
          if j == context:
            index2 = rand_index[i] 
          for k in xrange(size):
            vectors[index1, k] -= alpha * dx1[i, cur_col + k]
            vectors[index2, k] -= alpha * dx2[i, cur_col + k]

      current_row += batch


@cython.boundscheck(False)
@cython.wraparound(False)
cdef DTYPE_t batch_train(DTYPE_t [:, ::1] x1,
                         DTYPE_t [:, ::1] x2,
                         DTYPE_t [:, ::1] W1,
                         DTYPE_t [:, ::1] W2,
                         int batch,
                         int width,
                         int hidden,
                         DTYPE_t [:, ::1] dW1,
                         DTYPE_t [:, ::1] dW2,
                         DTYPE_t [:, ::1] dx1,
                         DTYPE_t [:, ::1] dx2,
                         DTYPE_t [:, ::1] v1,
                         DTYPE_t [:, ::1] v2,
                         DTYPE_t [:, ::1] diff,
                         DTYPE_t [:, ::1] tmp2,
                         DTYPE_t [:, ::1] tanh_err1,
                         DTYPE_t [:, ::1] tanh_err2) nogil:

    tanh_err1[:, :] = 0.0
    tanh_err2[:, :] = 0.0
    cdef DTYPE_t hinge = 0.0
    cdef DTYPE_t tmp = 0.0
    cdef int m = 0
    cdef int n = 0
    cdef int k = 0
    cdef int i = 0
    cdef int j = 0

    m = batch
    k = width
    n = hidden
    # np.dot(x1, W1)
    cdgemm(101, 111, 111, m, n, k, 1.0, &x1[0,0], k, &W1[0,0], n, 0.0, &v1[0,0], n)

    # np.dot(x2, W2)
    cdgemm(101, 111, 111, m, n, k, 1.0, &x2[0,0], k, &W1[0,0], n, 0.0, &v2[0,0], n)

    # v1 = np.tanh(v1)
    # v2 = np.tanh(v2)
    for i in xrange(batch):
      for j in xrange(hidden):
        v1[i,j] = fasttanh(v1[i, j])
        v2[i,j] = fasttanh(v2[i, j])

    # diff = v2 - v1
    for i in xrange(batch):
      for j in xrange(hidden):
        diff[i, j] = v2[i,j] - v1[i,j]

    m = batch
    k = hidden
    n = 1
    # np.dot(diff, W2)
    cdgemm(101, 111, 111, m, n, k, 1.0, &diff[0,0], k, &W2[0,0], n, 0.0, &tmp2[0,0], n)


    for i in xrange(batch):
      tmp2[i, 0] += 1.0
      if tmp2[i, 0] > 0:
        hinge += tmp2[i, 0]

    hinge /= batch


    for i in xrange(hidden):
      dW2[i] = 0.0

    for i in xrange(batch):
      if tmp2[i, 0] > 0:
        for j in xrange(hidden):
          dW2[j, 0] += diff[i][j] / batch

    for i in xrange(batch):
      if tmp2[i, 0] > 0:
        for j in xrange(hidden):
          tmp = W2[j, 0] / batch
          tanh_err1[i, j] = (1.0 - v1[i,j]*v1[i,j]) * tmp
          tanh_err2[i, j] = (1.0 - v2[i,j]*v2[i,j]) * tmp

    m = width
    k = batch
    n = hidden
    # dW1 = - np.dot(x1.T, tanh_err1)
    cdgemm(101, 112, 111, m, n, k, -1.0, &x1[0,0], m, &tanh_err1[0,0], n, 0.0, &dW1[0,0], n)

    # dW1 = np.dot(x2.T, tanh_err2) + dW1
    cdgemm(101, 112, 111, m, n, k, 1.0, &x2[0,0], m, &tanh_err2[0,0], n, 1.0, &dW1[0,0], n)


    m = batch
    k = hidden
    n = width
    # np.dot(tanh_err1, W1.T)
    cdgemm(101, 111, 112, m, n, k, -1.0, &tanh_err1[0,0], k, &W1[0,0], k, 0.0, &dx1[0,0], n)
    
    # np.dot(tanh_err2, W1.T)
    cdgemm(101, 111, 112, m, n, k, 1.0, &tanh_err2[0,0], k, &W1[0,0], k, 0.0, &dx2[0,0],  n)

    return hinge

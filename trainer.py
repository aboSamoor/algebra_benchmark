#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""template.py: Description of what the module does."""

from argparse import ArgumentParser
import logging
import sys
from io import open
from os import path
from time import time
from glob import glob

__author__ = "Rami Al-Rfou"
__email__ = "rmyeid@gmail.com"

LOGFORMAT = "%(asctime).19s %(levelname)s %(filename)s: %(lineno)s %(message)s"

import polyglot
from Polyglot import Polyglot
import numpy as np
from random import shuffle


class LineSentence(object):
    def __init__(self, source):
        """Simple format: one sentence = one line; words already preprocessed and separated by whitespace.

        source can be either a string or a file object

        Thus, one can use this for just plain files:

        sentences = LineSentence('myfile.txt')

        Or for compressed files:

        sentences = LineSentence(bz2.BZ2File('compressed_text.bz2'))
        """
        self.sources = glob(source)
        shuffle(self.sources)

    def __iter__(self):
        """Iterate through the lines in the source."""
        try:
            # Assume it is a file-like object and try treating it as such
            # Things that don't have seek will trigger an exception
            self.source.seek(0)
            for line in self.source:
                yield line.split()
        except AttributeError:
            # If it didn't work like a file, use it as a string filename
            for source in self.sources:
              for line in open(source):
                l = line.strip()
                if not l: continue
                if l.startswith('[['): continue
                if l.endswith('[['): continue
                yield l.split()


def main(args):
  sentences = LineSentence(args.files)
  model = Polyglot(sentences=sentences, vocab_file=args.vocab, size=args.size, alpha=args.alpha,
                   hidden=args.hidden, window=args.window, batch_size=args.batch_size,
                   min_count=args.min_count, workers=args.workers)
  model.save_word2vec_format(fname=args.output+'.model')

if __name__ == "__main__":
  parser = ArgumentParser()
  parser.add_argument("--files", dest="files", help="Corpus file[s]")
  parser.add_argument("--vocab", dest="vocab", help="Vocab that is list of"
                      " words with the their frequencies")
  parser.add_argument("--output", dest="output", help="Model text file output")
  parser.add_argument("--size", dest="size", help="Embedding size", type=int,
                      default=64)
  parser.add_argument("--alpha", dest="alpha", help="Initial learning rate", type=float,
                      default=0.025)
  parser.add_argument("--hidden", dest="hidden", help="Hidden layer size", type=int,
                      default=32)
  parser.add_argument("--window", dest="window", help="Context window size", type=int,
                      default=2)
  parser.add_argument("--batch", dest="batch_size", help="Batch size", type=int,
                      default=16)
  parser.add_argument("--min-count", dest="min_count", help="Minimum Count", type=int,
                      default=1)
  parser.add_argument("--workers", dest="workers", help="Number of Workers", type=int, default=1)
  parser.add_argument("-l", "--log", dest="log", help="log verbosity level", default="INFO")
  args = parser.parse_args()
  if args.log == 'DEBUG':
    sys.excepthook = debug
  numeric_level = getattr(logging, args.log.upper(), None)
  logging.basicConfig(level=numeric_level, format=LOGFORMAT)
  main(args)

import numpy as np
cimport numpy as np
cimport cython
from cython.view cimport array as cvarray


DTYPE = np.double
ctypedef np.double_t DTYPE_t
ctypedef np.int_t ITYPE_t


@cython.boundscheck(False)
@cython.wraparound(False)
def matrix_mul1(np.ndarray[DTYPE_t, ndim=2] A, 
                np.ndarray[DTYPE_t, ndim=2] B,
                np.ndarray[DTYPE_t, ndim=2] C):
  cdef:
    unsigned int num_rows = A.shape[0]
    unsigned int tmp = A.shape[1]
    unsigned int num_cols = B.shape[1]
    unsigned int i, j, k
  for i in xrange(num_rows):
    for j in xrange(num_cols):
      for k in xrange(tmp):
        C[i,j] += A[i, k] * B[k, j]
  C = np.asarray(C)


@cython.boundscheck(False)
@cython.wraparound(False)
def matrix_mul2(DTYPE_t[:, ::1] A,
                DTYPE_t[:, ::1] B,
                DTYPE_t[:, ::1] C):
  cdef:
    unsigned int num_rows = A.shape[0]
    unsigned int tmp = A.shape[1]
    unsigned int num_cols = B.shape[1]
    unsigned int i, j, k
  for i in xrange(num_rows):
    for j in xrange(num_cols):
      for k in xrange(tmp):
        C[i,j] += A[i, k] * B[k, j]
  C = np.asarray(C)


@cython.boundscheck(False)
@cython.wraparound(False)
def matrix_mul3(DTYPE_t[:, ::1] A,
                DTYPE_t[::1, :] B,
                DTYPE_t[:, ::1] C):
  cdef:
    unsigned int num_rows = A.shape[0]
    unsigned int tmp = A.shape[1]
    unsigned int num_cols = B.shape[1]
    unsigned int i, j, k
  for i in xrange(num_rows):
    for j in xrange(num_cols):
      for k in xrange(tmp):
        C[i,j] += A[i, k] * B[k, j]
  C = np.asarray(C)



cdef extern from "cblas.h":
    double ddot "cblas_ddot"(int N, double *X, int incX,double *Y, int incY)
    
    double cdgemm "cblas_dgemm"(char Order, 
                                char TransA,
                                char TransB,
                                int M, int N,
                                int K, double alpha, double *A, 
                                int lda, double *B, int ldb,
                                double beta, double *C, int ldc) nogil

    void cdaxpy "cblas_daxpy"(int N,
                              double alpha,
                              double *X,
                              int incX,
                              double *Y,
                              int incY)



@cython.boundscheck(False)
@cython.wraparound(False)
def blas2(DTYPE_t[:, ::1] A,
          DTYPE_t[:, ::1] B,
          DTYPE_t[:, ::1] C):
  cdef:
    int m = A.shape[0]
    int k = A.shape[1]
    int n = B.shape[1]
  cdgemm(101, 111, 111, m, n, k, 1.0, &A[0,0], k, &B[0,0], n, 0.0, &C[0,0], n)


@cython.boundscheck(False)
@cython.wraparound(False)
def matrix_inplace_sum(DTYPE_t[:, ::1] A,
                       DTYPE_t[:, ::1] B,
                       DTYPE_t c):
  cdef:
    unsigned int num_rows = A.shape[0]
    unsigned int num_cols = A.shape[1]
  for i in xrange(num_rows):
    for j in xrange(num_cols):
      A[i,j] += c*B[i,j]

@cython.boundscheck(False)
@cython.wraparound(False)
def blas_inplace_sum(DTYPE_t[:, ::1] A,
                       DTYPE_t[:, ::1] B,
                       DTYPE_t c):
  cdef int size = A.size

  cdaxpy(size, c, &B[0,0], 1, &A[0,0], 1)

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013 Rami Al-Rfou' <ralrfou@cs.stonybrook.edu>
# Licensed under the GNU LGPL v2.1 - http://www.gnu.org/licenses/lgpl.html


"""
Deep learning via word2vec's "hierarchical softmax skip-gram model" [1]_.
"""

import logging
import sys
import os
import heapq
import time
import itertools
import threading
from Queue import Queue
from io import open

from numpy import asarray, exp, dot, outer, random, dtype, get_include, float64 as REAL,\
    uint32, seterr, array, uint8, vstack, argsort, fromstring, sqrt, newaxis, \
    ndarray, zeros, prod, int64

logger = logging.getLogger("gensim.models.Polyglot")


from gensim import utils  # utility fnc for pickling, common scipy operations etc

#from os import environ
#environ['LDFLAGS'] = '-lblas'
#import pyximport
#pyximport.install(setup_args={"include_dirs": get_include()})
from polyglot import train as fast_train


def zeros_aligned(shape, dtype_, order='C', align=128):
    """Like `numpy.zeros()`, but the array will be aligned at `align` byte boundary."""
    nbytes = prod(shape, dtype=int64) * dtype(dtype_).itemsize
    buffer = zeros(nbytes + align, dtype=uint8)
    start_index = -buffer.ctypes.data % align
    return buffer[start_index : start_index + nbytes].view(dtype_).reshape(shape, order=order)


class Vocab(object):
    """A single vocabulary item, used internally for constructing binary trees (incl. both word leaves and inner nodes)."""
    def __init__(self, **kwargs):
        self.count = 0
        self.__dict__.update(kwargs)

    def __lt__(self, other):  # used for sorting in a priority queue
        return self.count < other.count

    def __str__(self):
        vals = ['%s:%r' % (key, self.__dict__[key]) for key in sorted(self.__dict__) if not key.startswith('_')]
        return "<" + ', '.join(vals) + ">"


class Polyglot(utils.SaveLoad):
    """
    Class for training, using and evaluating neural networks described in https://code.google.com/p/word2vec/

    The model can be stored/loaded via its `save()` and `load()` methods, or stored/loaded in a format
    compatible with the original word2vec implementation via `save_word2vec_format()` and `load_word2vec_format()`.

    """
    def __init__(self, sentences=None, vocab_file=None, size=100, alpha=0.025, hidden=32,
                 window=5, batch_size=16, min_count=5, seed=1, workers=1, min_alpha=0.0001):
        """
        Initialize the model from an iterable of `sentences`. Each sentence is a
        list of words (utf8 strings) that will be used for training.

        The `sentences` iterable can be simply a list, but for larger corpora,
        consider an iterable that streams the sentences directly from disk/network.
        See :class:`BrownCorpus`, :class:`Text8Corpus` or :class:`LineSentence` in
        this module for such examples.

        If you don't supply `sentences`, the model is left uninitialized -- use if
        you plan to initialize it in some other way.

        `size` is the dimensionality of the feature vectors.
        `window` is the maximum distance between the current and predicted word within a sentence.
        `alpha` is the initial learning rate (will linearly drop to zero as training progresses).
        `seed` = for the random number generator.
        `min_count` = ignore all words with total frequency lower than this.
        `workers` = use this many worker threads to train the model (=faster training with multicore machines)

        """
        self.vocab = {}  # mapping from a word (string) to a Vocab object
        self.index2word = []  # map from a word's matrix index (int) to word (string)
        self.size = int(size)
        self.alpha = float(alpha)
        self.context = int(window)
        self.index_width = 2 * self.context + 1
        self.width = self.index_width * self.size
        self.seed = seed
        self.hidden = hidden
        self.min_count = min_count
        self.workers = workers
        self.batch = batch_size
        self.min_alpha = min_alpha
        self.vocab_file = vocab_file
        if sentences is not None:
            self.build_vocab(sentences)
            self.train(sentences)

    def read_vocab_file(self):
      vocab = {}
      for i, line in enumerate(open(self.vocab_file)):
        l = line.strip()
        cols = l.split()

        if len(cols) == 1:
          if cols[0].isdigit():
            logging.warning("Missing word for the count {} at line {}".format(cols[0], i))
          else:
            vocab[word] = Vocab(count=1)

        if len(cols) == 2:
          count, word = cols
          if word in vocab:
            logging.warning("{} is duplicated in the vocabulary file".format(word))
          else:
            vocab[word] = Vocab(count=int(count))
      return vocab

    def build_vocab(self, sentences):
        """
        Build vocabulary from a sequence of sentences (can be a once-only generator stream).
        Each sentence must be a list of utf8 strings.

        """
        if self.vocab_file:
          vocab = self.read_vocab_file()
        else:
          vocab = self.count_vocab(sentences)
        # assign a unique index to each word
        self.vocab, self.index2word = {}, []
        for word in [u"<UNK>", u"<PAD>", u"<S>", u"</S>"]:
          v = Vocab(count=1)
          v.index = len(self.vocab)
          self.index2word.append(word)
          self.vocab[word] = v

        for word, v in vocab.iteritems():
            if v.count >= self.min_count:
                v.index = len(self.vocab)
                self.index2word.append(word)
                self.vocab[word] = v
        logger.info("total %i word types after removing those with count<%s" % (len(self.vocab), self.min_count))

        # add info about each word's Huffman encoding
        self.reset_weights()


    def count_vocab(self, sentences):
      logger.info("collecting all words and their counts")
      sentence_no, vocab = -1, {}
      total_words = 0
      for sentence_no, sentence in enumerate(sentences):
          if sentence_no % 100000 == 0:
              logger.info("PROGRESS: at sentence #%i, processed %i words and %i word types" %
                  (sentence_no, total_words, len(vocab)))
          for word in sentence:
              total_words += 1
              if word in vocab:
                  vocab[word].count += 1
              else:
                  vocab[word] = Vocab(count=1)
      logger.info("collected %i word types from a corpus of %i words and %i sentences" %
          (len(vocab), total_words, sentence_no + 1))
      return vocab


    def train(self, sentences, total_words=None, word_count=0, chunksize=1000):
        """
        Update the model's neural weights from a sequence of sentences (can be a once-only generator stream).
        Each sentence must be a list of utf8 strings.

        """
        logger.info("training model with %i workers on %i vocabulary and %i features" % (self.workers, len(self.vocab), self.size))

        # Notice that we want to pass jobs that are multiple of the batch size
        chunksize = chunksize * self.batch

        if not self.vocab:
            raise RuntimeError("you must first build vocabulary before training the model")

        start, next_report = time.time(), [1.0]
        word_count, total_words = [word_count], total_words or sum(v.count for v in self.vocab.itervalues())
        jobs = Queue(maxsize=2 * self.workers)  # buffer ahead only a limited number of jobs.. this is the reason we can't simply use ThreadPool :(
        lock = threading.Lock()  # for shared state (=number of words trained so far, log reports...)

        def worker_train():
            """Train the model, lifting lists of sentences from the jobs queue."""

            while True:
                job = jobs.get()
                if job is None:  # data finished, exit
                    break
                # update the learning rate before every job
                #alpha = max(self.min_alpha, self.alpha * (1 - 1.0 * word_count[0] / total_words))
                alpha = self.alpha
                # how many words did we train on? out-of-vocabulary (unknown) words do not count
                fast_train(self, job, alpha)
                job_words = job.shape[0]
                with lock:
                    word_count[0] += job_words
                    elapsed = time.time() - start
                    if elapsed >= next_report[0]:
                        logger.info("PROGRESS: at %.2f%% words, alpha %.05f, %.0f words/s" %
                            (100.0 * word_count[0] / total_words, alpha, word_count[0] / elapsed if elapsed else 0.0))
                        next_report[0] = elapsed + 1.0  # don't flood the log, wait at least a second between progress reports

        workers = [threading.Thread(target=worker_train) for _ in xrange(self.workers)]
        for thread in workers:
            thread.daemon = True  # make interrupting the process with ctrl+c easier
            thread.start()

        # convert input strings to Vocab objects (or None for OOV words), and start filling the jobs queue

        for job_no, job in enumerate(utils.grouper(self.iter_ngrams(sentences), chunksize)):
            logger.debug("putting job #%i in the queue, qsize=%i" % (job_no, jobs.qsize()))
            jobs.put(asarray(job, dtype='int32'))
        logger.info("reached the end of input; waiting to finish %i outstanding jobs" % jobs.qsize())
        for _ in xrange(self.workers):
            jobs.put(None)  # give the workers heads up that they can finish -- no more work!

        for thread in workers:
            thread.join()

        elapsed = time.time() - start
        logger.info("training on %i words took %.1fs, %.0f words/s" %
            (word_count[0], elapsed, word_count[0] / elapsed if elapsed else 0.0))

        return word_count[0]

    def iter_ngrams(self, sentences):
      ngram = []
      PAD_ID = self.vocab[u"<PAD>"].index
      unknown = self.vocab[u"<UNK>"]
      padding = [PAD_ID]*self.context
      sent_ = []
      context = self.context
      context_ = self.context + 1
      voc_get = self.vocab.get
      for i, sent in enumerate(sentences):
        len_ = len(sent)
        sent_.extend(padding)
        sent_.extend([voc_get(w, unknown).index for w in sent])
        sent_.extend(padding)
        for j in xrange(context, len_+context):
          yield sent_[j-context: j+context_]
        sent_ = []


    def reset_weights(self):
        """Reset all projection weights to an initial (untrained) state, but keep the existing vocabulary."""
        random.seed(self.seed)
        self.W1 = zeros_aligned((self.width, self.hidden), dtype_=REAL)
        self.W1 += self.random_values(self.W1.shape)
        self.W2 = zeros_aligned((self.hidden, 1), dtype_=REAL)
        self.W2 += self.random_values(self.W2.shape)
        self.vectors = zeros_aligned((len(self.vocab), self.size), dtype_=REAL)
        self.vectors += self.random_values(self.vectors.shape)
        if 'the' in self.vocab:
          logging.debug("We initialized the <the> vector with")
          logging.debug(self.vectors[self.vocab["the"].index])

    def random_values(self, shape):
      random_generator = random.RandomState(self.seed)
      total_dimensions = sum(shape)
      low = -sqrt(6./total_dimensions)
      high = sqrt(6./total_dimensions)
      random_values = random_generator.uniform(low=low, high=high, size=shape)
      W_values = asarray(random_values, dtype=type)
      return W_values
        

    def save_word2vec_format(self, fname, binary=False):
        """
        Store the input-hidden weight matrix in the same format used by the original
        C word2vec-tool, for compatibility.

        """
        logger.info("storing %sx%s projection weights into %s" % (len(self.vocab), self.size, fname))
        assert (len(self.vocab), self.size) == self.vectors.shape
        with open(fname, 'w') as fout:
            fout.write(u"%s %s\n" % self.vectors.shape)
            # store in sorted order: most frequent words at the top
            for word, vocab in sorted(self.vocab.iteritems(), key=lambda item: -item[1].count):
                row = self.vectors[vocab.index]
                if binary:
                    fout.write(u"%s %s\n" % (word, row.tostring()))
                else:
                    fout.write(u"%s %s\n" % (word, u' '.join(u"%f" % val for val in row)))


    @classmethod
    def load_word2vec_format(cls, fname, binary=False):
        """
        Load the input-hidden weight matrix from the original C word2vec-tool format.

        Note that the information loaded is incomplete (the binary tree is missing),
        so while you can query for word similarity etc., you cannot continue training
        with a model loaded this way.

        """
        logger.info("loading projection weights from %s" % (fname))
        with open(fname) as fin:
            header = fin.readline()
            vocab_size, size = map(int, header.split())  # throws for invalid file format
            result = Polyglot(size=size)
            result.vectors = zeros_aligned((vocab_size, size), dtype_=REAL)
            if binary:
                binary_len = dtype(REAL).itemsize * size
                for line_no in xrange(vocab_size):
                    # mixed text and binary: read text first, then binary
                    word = []
                    while True:
                        ch = fin.read(1)
                        if ch == u' ':
                            word = u''.join(word)
                            break
                        if ch != u'\n':  # ignore newlines in front of words (some binary files have newline, some not)
                            word.append(ch)
                    result.vocab[word] = Vocab(index=line_no, count=vocab_size - line_no)
                    result.index2word.append(word)
                    result.vectors[line_no] = fromstring(fin.read(binary_len), dtype=REAL)
            else:
                for line_no, line in enumerate(fin):
                    parts = line.split()
                    assert len(parts) == size + 1
                    word, weights = parts[0], map(REAL, parts[1:])
                    result.vocab[word] = Vocab(index=line_no, count=vocab_size - line_no)
                    result.index2word.append(word)
                    result.vectors[line_no] = weights
        logger.info("loaded %s matrix from %s" % (result.vectors.shape, fname))
        return result

    def __getitem__(self, word):
        """
        Return a word's representations in vector space, as a 1D numpy array.

        Example::

          >>> trained_model['woman']
          array([ -1.40128313e-02, ...]

        """
        return self.vectors[self.vocab[word].index]


    def __contains__(self, word):
        return word in self.vocab


    def __str__(self):
      return "Polyglot(vocab=%s, size=%s, alpha=%s)" % (len(self.index2word), self.layer1_size, self.alpha)

    def norm(self, matrix, by='row'):
      if by == 'row':
        norms = (matrix ** 2).sum(axis=1) ** 0.5
        return (matrix.T / norms).T

    def most_similar(self, word, neighbors=10):
      index = self.vocab[word].index
      normvecs = self.norm(self.vectors)
      point = normvecs[index]
      distances = ((normvecs - point) ** 2).sum(axis=1) ** 0.5
      best = distances.argsort()[:neighbors]
      for index in best:
        l = "{:<20}{:<10}".format(self.index2word[index], distances[index])
        print l.encode("utf-8")
      
